package me.inscription.hw_1;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    void textCopy() {
        TextView tv1 = (TextView)findViewById(R.id.textView);
        TextView tv2 = (TextView)findViewById(R.id.tvTada);
        EditText et1 = (EditText)findViewById(R.id.etTypeHere);

        if (tv2.getVisibility() != View.VISIBLE) tv2.setVisibility(View.VISIBLE);

        if (TextUtils.isEmpty(et1.getText())){
            //Если в et1 нет есть текста
            tv2.setText(getString(R.string.str_ouch));
            String str = getString(R.string.str_typed_nothing);
            tv1.setText(str);
        }
        else {
            //Если есть
            tv2.setText(getString(R.string.str_tada));
            String str = getString(R.string.str_typed) + "\n"
                    + "\"" + et1.getText().toString() + "\"";
            tv1.setText(str);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText.OnEditorActionListener etListener = new EditText.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) ||
                        (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    textCopy();
                }
                return false;
            }
        };

        EditText etTypeHere = (EditText)findViewById(R.id.etTypeHere);
        etTypeHere.setOnEditorActionListener(etListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        InputMethodManager imm1 = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        textCopy();
    }
}
